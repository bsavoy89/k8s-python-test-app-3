#Courtesy of Bradley Savoy - brad.savoy@gmail.com
FROM fc8dockq03.gfoundries.com:5000/centos:latest
WORKDIR /usr/src/app
RUN echo "proxy=http://uswwwp1.gfoundries.com:74" >> /etc/yum.conf
ENV HTTP_PROXY=uswwwp1.gfoundries.com:74
ENV HTTPS_PROXY=uswwwp1.gfoundries.com:74

#Update an install some tools I like to have around
RUN yum update -y
RUN yum install net-tools tcpdump traceroute python -y

#Grab the pip install script then install necessary python packages
RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
RUN python get-pip.py
RUN pip install --upgrade setuptools
RUN pip --no-cache-dir install web.py flask

#Copy the python script into the working directory
COPY ./pytest.py ./

#Clean up proxy settings (can cause issues reaching hosts)
ENV HTTP_PROXY=""
ENV HTTPS_PROXY=""
#Command to start the container - could use docker-entrypoint.sh instead but this is simpler
CMD ["python", "./pytest.py"]

#Port 8080 of the container should be exposed - this allos us to map a host port to the docker container
EXPOSE 8080
