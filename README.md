#### Bradley Savoy
Simple python application to test a couple of facets of a k8s system

1 - Build of docker container - This can be cloned into a jenkins pipeline then the docker container can be built with relative ease

	The application allows you to access 4 different URLs - 
	
		/ - displays the IP of the container you've reached
		
		/healthcheck - 5 second delay before returning string "ok"
		
		/custommessage - displays some silly text
		
		/listclusterpods - if in k8s cluster - will try to list all pods in cluster - note that this expects an insecure api server and you may need to adjust api server address
		


2 - Kubernetes deployment - This can be deployed to a kubernetes cluster with the pytest_deployment.yaml

	This includes a service, deployment, as well as a pod autoscaler
	
3 - Ingress - Demonstrate the ability o an application to be accessible from outside the cluster 
	
	pytest service should be reachable at <cluster ip>:31872 - regardless of which node you try to accesss from
	
4 - Horizontal Scaling - Demonstrate the ability of an application to scale transparently 

	Will scale once a pod reaches 75% cpu utilization
	
5 - Rolling update - Demonstrate the ability of an application to update automatically

	You can demonstrate a rolling update by simply changing the message in the pytest.py file and rebuilding the CI/CD pipeline in jenkins


TODO

Container Network - Test the ability of an application to communicate with another service in the cluster (a database)

Implement deployment objects with Helm

Push Jenkinsfile

Try to discover cluster api server